# Informasi Penerbangan dari Bandara CGK

## Daftar Isi
- [Panduan Saat Pandemi](#panduan-saat-pandemi)
  * [Jenis Tes](#jenis-tes)
  * [Aplikasi Kesehatan](#aplikasi-kesehatan)
  * [Perubahan New Normal](#perubahan-new-normal)
  * [Personal Protective Equipment](#personal-protective-equipment)
- [Panduan Umum](#panduan-umum)
  * [Pilih Terminal](#pilih-terminal)
  * [Transportasi ke Bandara](#transportasi-ke-bandara)
- [Penerbangan Domestik](#penerbangan-domestik)
  * [Saat di Bandara](#saat-di-bandara)
- [Penerbangan Internasional](#penerbangan-internasional)
  * [Sebelum Berangkat](#sebelum-berangkat)
    + [Pembuatan Paspor](#pembuatan-paspor)
    + [Visa](#visa)
      - [Panduan Umum](#panduan-umum-1)
      - [Panduan Spesifik](#panduan-spesifik)
  * [Saat di Bandara](#saat-di-bandara-1)
  * [Tips Spesifik Negara Tujuan](#tips-spesifik-negara-tujuan)
    + [Transportasi Umum](#transportasi-umum)
    + [Lain-Lain](#lain-lain)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Panduan Saat Pandemi

### Jenis Tes
Lihat [di sini](https://gist.github.com/iamdejan/73f212700e346fd900fa82cb9e1e5116#methods).

### Aplikasi Kesehatan
- Indonesia
    - [PeduliLindungi](https://play.google.com/store/apps/details?id=com.telkom.tracencare) (untuk perjalanan domestik)
- Singapura
    - [TraceTogether](https://play.google.com/store/apps/details?id=sg.gov.tech.bluetrace&hl=en&gl=US)
- Prancis
    - [TousAntiCovid](https://www.thelocal.fr/20210420/france-moves-towards-health-pass-with-vaccination-and-test-certificates-on-covid-tracking-app/)
- Belanda
    - [CoronaCheck](https://coronacheck.nl/en/)
- Jerman
    - [Corona Warn](https://www.coronawarn.app/en/)

### Perubahan New Normal
- Pesan makanan melalui aplikasi maskapai
    - Singapore Airlines
    - Air Asia
    - Qatar Airways

### Personal Protective Equipment
- Wajib
    - Masker
- Opsional
    - Hand sanitizer (tersedia di bandara)
    - Kaca mata transparan
    - Face shield (tidak perlu kalau sudah pakai kacamata transparan)

## Panduan Umum

### Pilih Terminal
[Daftar maskapai di masing-masing terminal](https://blog.tiket.com/panduan-terminal-maskapai-di-bandara-soekarno/)

### Transportasi ke Bandara
- [Cara Mudah Naik Kereta Bandara, Penerbangan Pagi Nggak Jadi Masalah!](https://blog.tiket.com/cara-naik-kereta-bandara/)

## Penerbangan Domestik

### Saat di Bandara
- [Pertama Kali Naik Pesawat? Ikuti Cara Naik Pesawat Pertama Kali Berikut Ini!](https://blog.tiket.com/cara-naik-pesawat-pertama-kali/)
- [CARA NAIK PESAWAT TERBANG PERTAMA KALI](https://www.youtube.com/watch?v=5ZvQofFEpDM) (tutorial saat pandemi)

## Penerbangan Internasional

### Sebelum Berangkat

#### Pembuatan Paspor
- [Cara Membuat Paspor Online 2022 Terbaru, Lengkap dengan Biayanya](https://blog.tiket.com/cara-bikin-paspor-online/)

#### Visa

##### Panduan Umum
- [Macam-macam visa](https://www.genpi.co/travel/14657/paspor-bebas-visa-e-visa-visa-on-arrival-apa-artinya)
- [Startup Visa: Here’s 15 Countries that Offer the Startup Visa to Foreign Entrepreneurs](https://startupswb.com/startup-visa-heres-15-countries-that-offer-the-startup-visa-to-foreign-entrepreneurs.html)

##### Panduan Spesifik
- [Visa Schengen](https://blog.tiket.com/cara-membuat-visa-schengen/) (untuk turis)
- [French Tech Visa](https://lafrenchtech.com/en/how-france-helps-startups/french-tech-visa/)
- [Germany Working Visa](https://www.germany-visa.org/work-employment-visa/)
- [Netherlands work permit](https://www.iamexpat.nl/expat-info/official-issues/work-permit-netherlands)
- [Netherlands Start-up Visa](https://ind.nl/en/work/working_in_the_Netherlands/Pages/Start-up.aspx)
- [Lithuania Startup Visa](https://startupvisalithuania.com/)
- [Estonia Startup Visa](https://startupestonia.ee/visa)
- [Latvia Startup Visa](https://startuplatvia.eu/startup-visa)
- [Singapore Employment Pass](https://www.mom.gov.sg/passes-and-permits/employment-pass)
- [Singapore Entre Pass](https://www.mom.gov.sg/passes-and-permits/entrepass)
- Pembuatan visa lain: tanya kedutaan negara tujuan di Jakarta.

### Saat di Bandara
- [PANDUAN CARA KEBERANGKATAN KELUAR NEGERI TKI BALIK CUTI, DI TERMINAL 3 BANDARA SOEKARNO HATTA](https://youtu.be/i0NtpmGnMc8)
- [Tutorial Cara Naik Pesawat Pertama Kali Ke Luar Negeri Bagi Pemula](https://www.youtube.com/watch?v=uTdDmjApw3Q)

### Tips Spesifik Negara Tujuan

#### Transportasi Umum
- [Moscow airport to city CHEAPLY - Buying Troika card and mobile sim card](https://www.youtube.com/watch?v=ahd8fGLsTf0)
- [Podorozhnik - St. Petersburg](http://podorozhnik.spb.ru/en/)
    - [Bus to and from Pulkovo Airport](https://pulkovoairport.ru/en/transport/bus/)
    - [Transportation to and from Saint Petersburg Pulkovo Airport](https://airmundo.com/en/airports/saint-petersburg-pulkovo-airport/transportation/)
- [NETS FlashPay - Singapore](https://www.nets.com.sg/personal/retail-payments/nets-flashpay/)
- [Navigo Easy - Paris](https://europeforvisitors.com/paris/articles/navigo-easy.htm)
- [OV Chipkaart - Amsterdam](https://www.youtube.com/watch?v=qEvwAdzULB0)
- [Munich](https://www.youtube.com/watch?v=7nYLgyyAfzA)
- [How To Travel Around London and Buy an Oyster Card - Important Tips!](https://www.youtube.com/watch?v=LlZ_xDx2Zl0)
- [A First Timer’s Guide to Using the Dubai NOL Card](https://www.headout.com/blog/nol-card-dubai/)
    - [Everything you need to know about the Dubai Metro](https://www.visitdubai.com/en/articles/guide-to-dubai-metro)

#### Lain-Lain
- [Hal Wajib Tahu Sebelum Naik Pesawat ke Taiwan](https://www.youtube.com/watch?v=whx3XXYJ7zE)
